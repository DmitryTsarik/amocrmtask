package com.raphanum.amocrmtask;

import android.app.Application;
import android.content.Context;

import com.raphanum.amocrmtask.di.AppComponent;
import com.raphanum.amocrmtask.di.DaggerAppComponent;
import com.raphanum.amocrmtask.di.NetworkModule;

public class App extends Application {
    private static AppComponent component;

    public static AppComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder()
                .networkModule(new NetworkModule(this))
                .build();
    }
}
