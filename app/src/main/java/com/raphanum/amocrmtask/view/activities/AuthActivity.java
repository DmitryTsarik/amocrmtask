package com.raphanum.amocrmtask.view.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.raphanum.amocrmtask.R;
import com.raphanum.amocrmtask.di.AuthViewComponent;
import com.raphanum.amocrmtask.di.AuthViewModule;
import com.raphanum.amocrmtask.di.DaggerAuthViewComponent;
import com.raphanum.amocrmtask.presenter.AuthPresenter;
import com.raphanum.amocrmtask.utils.Constants;
import com.raphanum.amocrmtask.view.AuthView;

import javax.inject.Inject;

public class AuthActivity extends AppCompatActivity implements AuthView {
    @Inject
    protected AuthPresenter presenter;
    private AuthViewComponent viewComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        initViewComponent();

        Button authButton = (Button) findViewById(R.id.auth_button);
        EditText loginEditText = (EditText) findViewById(R.id.login_edit_text);
        EditText hashEditText = (EditText) findViewById(R.id.hash_edit_text);
        // для проверки, чтобы не вводить каждый раз
        loginEditText.setText(Constants.USER_LOGIN);
        hashEditText.setText(Constants.API_KEY);

        authButton.setOnClickListener(view -> presenter.onAuth(loginEditText.getText().toString(),
                hashEditText.getText().toString()));
    }

    @Override
    public void showLeadListView() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void showLoading() {
        findViewById(R.id.auth_progress).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        findViewById(R.id.auth_progress).setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    private void initViewComponent() {
        if (viewComponent == null) {
            viewComponent = DaggerAuthViewComponent.builder()
                    .authViewModule(new AuthViewModule(this))
                    .build();
        }
        viewComponent.inject(this);
    }
}
