package com.raphanum.amocrmtask.view.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.raphanum.amocrmtask.R;
import com.raphanum.amocrmtask.model.parcelable.Lead;

import java.util.List;

public class LeadListAdapter extends RecyclerView.Adapter<LeadListAdapter.LeadViewHolder> {
    private List<Lead> leads;

    @Override
    public LeadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lead_item, parent, false);
        return new LeadViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LeadViewHolder holder, int position) {
        holder.name.setText(leads.get(position).getName());
        holder.creationDate.setText(leads.get(position).getCreationDate());
        holder.price.setText(leads.get(position).getPrice());
        holder.status.setText(leads.get(position).getStatus().getName());
        holder.status.setBackgroundColor(Color.parseColor(leads.get(position).getStatus().getColor()));
    }

    @Override
    public int getItemCount() {
        if (leads != null) {
            return leads.size();
        }
        return 0;
    }

    public void setLeads(List<Lead> leads) {
        this.leads = leads;
        notifyDataSetChanged();
    }

    static class LeadViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView creationDate;
        private TextView price;
        private TextView status;

        LeadViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.item_lead_name);
            creationDate = (TextView) itemView.findViewById(R.id.item_lead_creation_date);
            price = (TextView) itemView.findViewById(R.id.item_lead_price);
            status = (TextView) itemView.findViewById(R.id.item_lead_status);
        }
    }
}
