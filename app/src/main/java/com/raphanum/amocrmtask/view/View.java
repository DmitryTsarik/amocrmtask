package com.raphanum.amocrmtask.view;

public interface View {

    void showLoading();

    void hideLoading();

    void showError(String error);
}
