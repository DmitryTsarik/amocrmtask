package com.raphanum.amocrmtask.view;

import com.raphanum.amocrmtask.model.parcelable.Lead;

import java.util.List;

public interface LeadListView extends View {

    void showLeads(List<Lead> leads);

    void showEmpty();
}
