package com.raphanum.amocrmtask.presenter;

import com.raphanum.amocrmtask.App;
import com.raphanum.amocrmtask.model.Model;
import com.raphanum.amocrmtask.utils.NetworkManager;

import javax.inject.Inject;

public abstract class BasePresenter {
    @Inject
    protected Model model;
    @Inject
    protected NetworkManager networkManager;

    public BasePresenter() {
        App.getComponent().inject(this);
    }
}
