package com.raphanum.amocrmtask.presenter;

import android.util.Log;

import com.raphanum.amocrmtask.utils.Constants;
import com.raphanum.amocrmtask.view.LeadListView;

import java.io.IOException;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LeadListPresenter extends BasePresenter {
    private LeadListView view;

    @Inject
    public LeadListPresenter() {
    }

    public LeadListPresenter(LeadListView view) {
        this.view = view;
    }

    public void onRefresh() {
        loadLeadsWithStatuses();
    }

    private void loadLeadsWithStatuses() {
        view.showLoading();
        model.getLeadsWithStatuses()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(leads -> {
                            view.hideLoading();
                            if (leads.size() > 0) {
                                view.showLeads(leads);
                            } else {
                                view.showEmpty();
                            }
                        },
                        throwable -> {
                            view.hideLoading();
                            view.showError(throwable.getMessage());
                            throwable.printStackTrace();
                        });
    }

    public void onAuth() {
        networkManager.postAuth(Constants.USER_LOGIN, Constants.API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> {
                            view.showError("Auth success");
                            try {
                                Log.v("<===== Auth log =====>", responseBody.string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        },
                        throwable -> {
                            view.showError("Auth error");
                            throwable.printStackTrace();
                        });
    }
}
