package com.raphanum.amocrmtask.presenter;

import com.raphanum.amocrmtask.view.AuthView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AuthPresenter extends BasePresenter {
    private AuthView view;

    public AuthPresenter(AuthView view) {
        this.view = view;
    }

    public void onAuth(String login, String apiKey) {
        if (login.length() > 0 && apiKey.length() > 0) {
            view.showLoading();
            networkManager.postAuth(login, apiKey)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(responseBody -> {
                        view.hideLoading();
                        view.showLeadListView();
                    },
                            throwable -> {
                                view.hideLoading();
                                view.showError("Auth error");
                                throwable.printStackTrace();
                            });
        } else {
            view.showError("Auth error");
        }
    }
}
