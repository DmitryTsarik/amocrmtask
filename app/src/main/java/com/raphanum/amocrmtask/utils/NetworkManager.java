package com.raphanum.amocrmtask.utils;

import android.util.Log;

import com.raphanum.amocrmtask.App;
import com.raphanum.amocrmtask.model.api.AmoCrmApi;
import com.raphanum.amocrmtask.model.dto.AccountRootDTO;
import com.raphanum.amocrmtask.model.dto.LeadsRootDTO;

import java.util.List;

import javax.inject.Inject;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class NetworkManager {
    private AmoCrmApi api;
    @Inject
    protected AddCookiesInterceptor addCookiesInterceptor;
    @Inject
    protected ReceivedCookiesInterceptor receivedCookiesInterceptor;

    public NetworkManager() {
        App.getComponent().inject(this);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(addCookiesInterceptor)
                .addInterceptor(receivedCookiesInterceptor)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        api = retrofit.create(AmoCrmApi.class);
    }

    public Observable<LeadsRootDTO> getLeads() {
        return api.getLeads();
    }

    public Observable<AccountRootDTO> getAccountData() {
        return api.getAccountData();
    }

    public Observable<ResponseBody> postAuth(String login, String apiKey) {
        return api.postAuth(login, apiKey, Constants.AUTH_RESPONSE_TYPE);
    }
}
