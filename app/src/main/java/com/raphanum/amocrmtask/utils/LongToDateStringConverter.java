package com.raphanum.amocrmtask.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class LongToDateStringConverter {

    public static String convert(long date) {
        date *= 1000;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH);
        return dateFormat.format(new Date(date));
    }
}
