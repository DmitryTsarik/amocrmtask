package com.raphanum.amocrmtask.di;

import com.raphanum.amocrmtask.presenter.AuthPresenter;
import com.raphanum.amocrmtask.view.AuthView;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthViewModule {
    private AuthView view;

    public AuthViewModule(AuthView view) {
        this.view = view;
    }

    @Provides
    AuthPresenter providesAuthPresenter() {
        return new AuthPresenter(view);
    }
}
