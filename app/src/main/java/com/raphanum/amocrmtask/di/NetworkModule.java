package com.raphanum.amocrmtask.di;

import android.content.Context;

import com.raphanum.amocrmtask.utils.AddCookiesInterceptor;
import com.raphanum.amocrmtask.utils.ReceivedCookiesInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NetworkModule {
    private Context context;

    public NetworkModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    AddCookiesInterceptor providesAddCookiesInterceptor() {
        return new AddCookiesInterceptor(context);
    }

    @Provides
    @Singleton
    ReceivedCookiesInterceptor providesReceivedCookiesInterceptor() {
        return new ReceivedCookiesInterceptor(context);
    }
}
