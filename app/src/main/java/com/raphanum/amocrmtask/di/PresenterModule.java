package com.raphanum.amocrmtask.di;

import com.raphanum.amocrmtask.model.Model;
import com.raphanum.amocrmtask.model.ModelImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PresenterModule {

    @Provides
    @Singleton
    Model provideModel() {
        return new ModelImpl();
    }
}
