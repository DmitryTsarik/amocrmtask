package com.raphanum.amocrmtask.di;

import com.raphanum.amocrmtask.view.activities.AuthActivity;

import dagger.Component;

@Component(modules = {AuthViewModule.class})
public interface AuthViewComponent {

    void inject(AuthActivity activity);
}
