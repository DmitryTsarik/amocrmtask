package com.raphanum.amocrmtask.di;

import com.raphanum.amocrmtask.model.ModelImpl;
import com.raphanum.amocrmtask.presenter.BasePresenter;
import com.raphanum.amocrmtask.utils.NetworkManager;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {PresenterModule.class, ModelModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(BasePresenter presenter);

    void inject(ModelImpl model);

    void inject(NetworkManager networkManager);
}
