package com.raphanum.amocrmtask.model.api;

import com.raphanum.amocrmtask.model.dto.AccountRootDTO;
import com.raphanum.amocrmtask.model.dto.LeadsRootDTO;

import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface AmoCrmApi {

    @GET("private/api/v2/json/leads/list")
    Observable<LeadsRootDTO> getLeads();

    @GET("private/api/v2/json/accounts/current")
    Observable<AccountRootDTO> getAccountData();

    @POST("private/api/auth.php")
    @FormUrlEncoded
    Observable<ResponseBody> postAuth(@Field("USER_LOGIN") String login,
                                      @Field("USER_HASH") String apiKey,
                                      @Query("type") String type);
}
