package com.raphanum.amocrmtask.model.dto;

public class LeadStatusDTO {
    private String id;
    private String name;
    private String color;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
}
