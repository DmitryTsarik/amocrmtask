package com.raphanum.amocrmtask.model;

import com.raphanum.amocrmtask.App;
import com.raphanum.amocrmtask.model.dto.LeadDTO;
import com.raphanum.amocrmtask.model.dto.LeadStatusDTO;
import com.raphanum.amocrmtask.model.parcelable.Lead;
import com.raphanum.amocrmtask.model.parcelable.Status;
import com.raphanum.amocrmtask.utils.Constants;
import com.raphanum.amocrmtask.utils.LeadsInfoCombiner;
import com.raphanum.amocrmtask.utils.NetworkManager;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import rx.Observable;

public class ModelImpl implements Model {
    @Inject
    protected NetworkManager networkManager;

    public ModelImpl() {
        App.getComponent().inject(this);
    }

    @Override
    public Observable<List<Lead>> getLeadsWithStatuses() {
        return Observable.combineLatest(getObservableLeadList(),
                getObservableStatusesMap(),
                new LeadsInfoCombiner());
    }

    private Observable<List<LeadDTO>> getObservableLeadList() {
        return networkManager.getLeads()
                .map(root -> root.getResponse().getLeads());
    }

    private Observable<HashMap<String, Status>> getObservableStatusesMap() {
        return networkManager.getAccountData()
                .map(accountRootDTO -> accountRootDTO.getResponse().getAccount().getLeadsStatuses())
                .map(leadStatusDTOs -> {
                    HashMap<String, Status> statuses = new HashMap<>();
                    for (LeadStatusDTO status : leadStatusDTOs) {
                        statuses.put(status.getId(), new Status(status.getName(), status.getColor()));
                    }
                    return statuses;
                });
    }
}
